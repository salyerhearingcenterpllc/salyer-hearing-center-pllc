For more than 30 years, Salyer Hearing Center has established the regional gold standard for hearing evaluations, fittings and service. Our audiologists utilize state of the art technology in assessing and treating hearing loss.

Address: 166 Holly Springs Park Dr, Franklin, NC 28734, USA

Phone: 828-524-5599
